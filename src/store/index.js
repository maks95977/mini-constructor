import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const API_URL = 'https://api.themoviedb.org/3/';
const API_KEY = '242f820695932060c0c64cefd34e991a';

const axios = require('axios').default;

export default new Vuex.Store({
  state: {
    currentSite: [],
    uniqId: 0,
  },
  getters: {
    currentSite: (state) => state.currentSite,
  },
  mutations: {
    addBlockOnSite: (state, payload) => {
      console.log(payload);
      let newBlock = {};
      if (payload.type === 'defaultBlock') {
        newBlock = {
          type: payload.type,
          id: state.uniqId,
          content: {
            header: {
              text: 'Заголовок',
              styleType: 'default',
            },
            description: {
              text: 'Описание',
              styleType: 'default',
            },
          },
        };
      } else if (payload.type === 'cardsBlock') {
        newBlock = {
          type: payload.type,
          id: state.uniqId,
          cards: [
            {
              id: state.uniqId,
              content: {
                header: {
                  text: 'Заголовок карточки',
                  styleType: 'default',
                },
                description: {
                  text: 'Описание карточки',
                  styleType: 'default',
                },
              },
              icon: 'mdi-arrow-up-bold',
            },
          ],
        };
      } else if (payload.type === 'topMoviesBlock') {
        newBlock = payload;
        newBlock.id = state.uniqId;
        newBlock.type = payload.type;
        newBlock.newAmount = 1;
      }
      if (payload.type !== '') {
        state.currentSite.push(newBlock);
        state.uniqId += 1;
      }
      console.log(newBlock);
    },

    addCard: (state, payload) => {
      const newCard = {
        id: payload.id,
        content: {
          header: {
            text: 'Заголовок карточки',
            styleType: 'default',
          },
          description: {
            text: 'Описание карточки',
            styleType: 'default',
          },
        },
        icon: 'mdi-arrow-up-bold',
      };
      state.currentSite.find((o, i) => {
        if (o.id === payload.id) {
          state.currentSite[i].cards.push(newCard);
          return true;
        }
        return false;
      });
    },

    updateDefaultBlock: (state, payload) => {
      state.currentSite.find((o, i) => {
        if (o.id === payload.id) {
          const currentBlock = state.currentSite[i].content;
          currentBlock.header.text = payload.headerText;
          currentBlock.header.styleType = payload.headerStyleType;
          currentBlock.description.text = payload.description;
          currentBlock.description.styleType = payload.descriptionStyleType;
          return true;
        }
        return false;
      });
    },

    updateCardsBlock: (state, payload) => {
      state.currentSite.find((o) => {
        if (o.id === payload.id) {
          Object.keys(o.cards).forEach((element) => {
            if (parseInt(element, 32) === payload.index) {
              const currentCard = o.cards[payload.index];
              currentCard.content.header.text = payload.headerText;
              currentCard.content.header.styleType = payload.headerStyleType;
              currentCard.content.description.text = payload.description;
              currentCard.content.description.styleType = payload.descriptionStyleType;
              currentCard.icon = payload.icon;
            }
          });
        }
        return false;
      });
    },
    updateTopMoviesBlock: (state, payload) => {
      state.currentSite.find((o, i) => {
        if (o.id === payload.id) {
          const currentBlock = state.currentSite[i];
          currentBlock.newAmount = payload.moviesAmount;
          return true;
        }
        return false;
      });
    },

    updateCurrentSite: (state, payload) => {
      state.currentSite = payload;
    },

    deleteBlock: (state, payload) => {
      state.currentSite = state.currentSite.filter((blocks) => blocks.id !== payload.id);
    },

    deleteCard: (state, payload) => {
      state.currentSite.find((o) => {
        if (o.id === payload.id) {
          o.cards.splice(payload.index, 1);
        }
        return false;
      });
    },
  },
  actions: {
    addBlock: async (context, payload) => {
      if (payload.type === 'topMoviesBlock') {
        const url = `${API_URL}movie/popular?api_key=${API_KEY}&language=ru`;
        axios.get(url).then((response) => {
          const topMoviesPayload = response;
          topMoviesPayload.data.type = 'topMoviesBlock';
          context.commit('addBlockOnSite', topMoviesPayload.data);
        });
      } else {
        context.commit('addBlockOnSite', payload);
      }
    },

    addCard: async (context, payload) => {
      context.commit('addCard', payload);
    },

    updateBlock: async (context, payload) => {
      if (payload.type === 'defaultBlock') {
        context.commit('updateDefaultBlock', payload);
      } else if (payload.type === 'cardsBlock') {
        context.commit('updateCardsBlock', payload);
      } else {
        context.commit('updateTopMoviesBlock', payload);
      }
    },

    deleteBlock: async (context, payload) => {
      context.commit('deleteBlock', payload);
    },

    deleteCard: async (context, payload) => {
      context.commit('deleteCard', payload);
    },
  },
  modules: {},
});
